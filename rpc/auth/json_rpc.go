package auth

import (
	"context"

	"gitlab.com/jakinss321/microservice_auth/internal/modules/auth/service"
)

type AuthServiceJSONRPC struct {
	userService service.Auther
}

func NewAuthServiceJSONRPC(authService service.Auther) *AuthServiceJSONRPC {
	return &AuthServiceJSONRPC{userService: authService}
}

func (t *AuthServiceJSONRPC) Register(in service.RegisterIn, out *service.RegisterOut) error {
	*out = t.userService.Register(context.Background(), in, 1)
	return nil
}

func (t *AuthServiceJSONRPC) Login(in service.AuthorizeEmailIn, out *service.AuthorizeOut) error {
	*out = t.userService.AuthorizeEmail(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) Refresh(in service.AuthorizeRefreshIn, out *service.AuthorizeOut) error {
	*out = t.userService.AuthorizeRefresh(context.Background(), in)
	return nil
}

func (t *AuthServiceJSONRPC) VerifyEmail(in service.VerifyEmailIn, out *service.VerifyEmailOut) error {
	*out = t.userService.VerifyEmail(context.Background(), in)
	return nil
}
