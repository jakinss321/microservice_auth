package main

import (
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/jakinss321/microservice_auth/config"
	"gitlab.com/jakinss321/microservice_auth/internal/infrastructure/logs"
	"gitlab.com/jakinss321/microservice_auth/run"
)

// @title           Documentation of your project API.
// @version         1.0

// @contact.name   microservice_Auth

// @host      localhost:8082

func main() {
	// Загружаем переменные окружения из файла .env
	err := godotenv.Load()
	// Создаем конфигурацию приложения
	conf := config.NewAppConf()
	// Создаем логгер
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}
