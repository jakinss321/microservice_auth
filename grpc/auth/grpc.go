package auth

import (
	context "context"

	"gitlab.com/jakinss321/microservice_auth/internal/modules/auth/service"
)

type AuthServiceGRPC struct {
	userService service.Auther
}

func NewAuthServiceGRPC(authService service.Auther) *AuthServiceGRPC {
	return &AuthServiceGRPC{userService: authService}
}

func (s *AuthServiceGRPC) Register(ctx context.Context, in *RegisterRequest) (*RegisterResponse, error) {
	out := &RegisterResponse{}

	result := s.userService.Register(ctx, service.RegisterIn{
		Email:    in.Email,
		Phone:    in.Phone,
		Password: in.Password,
	}, 1)

	out.Status = int32(result.Status)
	out.ErrorCode = int32(result.ErrorCode)

	return out, nil
}

func (s *AuthServiceGRPC) Login(ctx context.Context, in *LoginRequest) (*AuthorizeOut, error) {
	result := s.userService.AuthorizeEmail(ctx, service.AuthorizeEmailIn{
		Email:    in.Email,
		Password: in.Password,
	})

	out := &AuthorizeOut{
		Id:           int32(result.UserID),
		Accesstoken:  result.AccessToken,
		Refreshtoken: result.RefreshToken,
		Errorcode:    int32(result.ErrorCode),
	}

	return out, nil
}

func (s *AuthServiceGRPC) Refresh(ctx context.Context, in *RefreshRequest) (*AuthorizeOut, error) {
	result := s.userService.AuthorizeRefresh(ctx, service.AuthorizeRefreshIn{
		UserID: int(in.Id),
	})

	out := &AuthorizeOut{
		Id:           int32(result.UserID),
		Accesstoken:  result.AccessToken,
		Refreshtoken: result.RefreshToken,
		Errorcode:    int32(result.ErrorCode),
	}

	return out, nil
}

func (s *AuthServiceGRPC) VerifyEmail(ctx context.Context, in *VerifyEmailRequest) (*VerifyEmailResponse, error) {
	out := &VerifyEmailResponse{}
	result := s.userService.VerifyEmail(ctx, service.VerifyEmailIn{
		Hash:  in.Email,
		Email: in.Hash,
	})

	out.Success = result.Success
	out.ErrorCode = int32(result.ErrorCode)

	return out, nil
}

func (s *AuthServiceGRPC) mustEmbedUnimplementedAuthServiceServer() {
	panic("")
}
