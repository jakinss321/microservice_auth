package storages

import (
	"gitlab.com/jakinss321/microservice_auth/internal/db/adapter"
	"gitlab.com/jakinss321/microservice_auth/internal/infrastructure/cache"
	vstorage "gitlab.com/jakinss321/microservice_auth/internal/modules/auth/storage"
)

type Storages struct {
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
