package modules

import (
	"gitlab.com/jakinss321/microservice_auth/internal/infrastructure/component"
	acontroller "gitlab.com/jakinss321/microservice_auth/internal/modules/auth/controller"
)

type Controllers struct {
	Auth acontroller.Auther
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	authController := acontroller.NewAuth(services.Auth, components)

	return &Controllers{
		Auth: authController,
	}
}
