package modules

import (
	"gitlab.com/jakinss321/microservice_auth/internal/infrastructure/component"
	aservice "gitlab.com/jakinss321/microservice_auth/internal/modules/auth/service"
	"gitlab.com/jakinss321/microservice_auth/internal/storages"
)

type Services struct {
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		Auth: aservice.NewAuth(components), //storages.Verify,
	}
}
