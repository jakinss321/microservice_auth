package service

import (
	"context"
)

type Auther interface {
	Register(ctx context.Context, in RegisterIn, field int) RegisterOut
	AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut
	AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut
	AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut
	SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut
	VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut
}

const (
	RegisterEmail = iota + 1
	RegisterPhone
)

type VerifyEmailIn struct {
	Hash  string
	Email string
}

type VerifyEmailOut struct {
	Success   bool
	ErrorCode int
}

type SendPhoneCodeIn struct {
	Phone string
}

type SendPhoneCodeOut struct {
	Phone string
	Code  int
}

type AuthorizeIn struct {
	Email    string
	Password string
}

type AuthorizeOut struct {
	UserID       int
	AccessToken  string
	RefreshToken string
	ErrorCode    int
}

type RegisterIn struct {
	Email          string
	Phone          string
	Password       string
	IdempotencyKey string
}

type RegisterOut struct {
	Status    int
	ErrorCode int
}

type UserCreateOut struct {
	UserID    int `json:"user_id"`
	ErrorCode int `json:"error_code"`
}

type AuthorizeEmailIn struct {
	Email          string
	Password       string
	RetypePassword string
}

type AuthorizeRefreshIn struct {
	UserID int
}

type AuthorizePhoneIn struct {
	Phone string
	Code  int
}

type Out struct {
	ErrorCode int
}

type UserCreateIn struct {
	Name     string `json:"name"`
	Phone    string `json:"phone"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Role     int    `json:"role"`
}

type GetByEmailIn struct {
	Email string `json:"email"`
}

type GetByIDIn struct {
	UserID int `json:"user_id"`
}

type UserVerifyEmailIn struct {
	UserID int `json:"user_id"`
}
