package service

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/rpc"
	"net/url"
	"strconv"
	"time"

	"net/rpc/jsonrpc"

	"gitlab.com/jakinss321/microservice_auth/config"
	"gitlab.com/jakinss321/microservice_auth/internal/infrastructure/component"
	"gitlab.com/jakinss321/microservice_auth/internal/infrastructure/errors"
	iservice "gitlab.com/jakinss321/microservice_auth/internal/infrastructure/service"
	"gitlab.com/jakinss321/microservice_auth/internal/infrastructure/tools/cryptography"
	"gitlab.com/jakinss321/microservice_auth/internal/models"
	"gitlab.com/jakinss321/microservice_auth/internal/modules/auth/storage"
	servicepb "gitlab.com/jakinss321/microservice_user/grpc/user"
	m "gitlab.com/jakinss321/microservice_user/pkg/models"
	"gitlab.com/jakinss321/microservice_user/pkg/modules/user/service"
	"google.golang.org/grpc"

	// uservice "gitlab.com/jakinss321/microservice_auth/internal/modules/user/service"
	"go.uber.org/zap"
)

type Auth struct {
	conf                  config.AppConf
	verify                storage.Verifier
	notify                iservice.Notifier
	tokenManager          cryptography.TokenManager
	hash                  cryptography.Hasher
	logger                *zap.Logger
	userJRPC              *rpc.Client
	userGRPC              *grpc.ClientConn
	userServiceClientGRPC servicepb.UserServiceClient
	Ifgrpc                bool
}

func NewAuth(components *component.Components) *Auth {
	var clientJRPC *rpc.Client
	var clientGRPC *grpc.ClientConn
	var userServiceClient servicepb.UserServiceClient
	var err error
	var Ifgrpc bool
	if components.Conf.RPCServer.GRPCorRPC == "GRPC" {
		Ifgrpc = true
		clientGRPC, err = grpc.Dial("userapi:3455", grpc.WithInsecure())
		if err != nil {
			log.Fatalf("failed to dial user grpc service: %v", err)
		}
		userServiceClient = servicepb.NewUserServiceClient(clientGRPC)
	} else {
		clientJRPC, err = jsonrpc.Dial("tcp", "userapi:3455")
		if err != nil {
			log.Fatalf("failed to dial user jrpc service: %v", err)
		}
	}

	return &Auth{
		conf:                  components.Conf,
		notify:                components.Notify,
		tokenManager:          components.TokenManager,
		hash:                  components.Hash,
		logger:                components.Logger,
		userJRPC:              clientJRPC,
		userGRPC:              clientGRPC,
		userServiceClientGRPC: userServiceClient,
		Ifgrpc:                Ifgrpc,
	}
}

func (a *Auth) Register(ctx context.Context, in RegisterIn, field int) RegisterOut {
	hashPass, err := cryptography.HashPassword(in.Password)
	if err != nil {
		return RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: errors.HashPasswordError,
		}
	}

	result := &service.UserCreateOut{}
	userCreate := service.UserCreateIn{
		Email:    in.Email,
		Password: hashPass,
	}
	user := &service.UserOut{}
	if a.Ifgrpc {
		userCreate := &servicepb.UserCreateIn{
			Email:    in.Email,
			Password: hashPass,
		}
		res, err := a.userServiceClientGRPC.CreateUser(ctx, userCreate)
		if err != nil {
			return RegisterOut{
				Status:    http.StatusInternalServerError,
				ErrorCode: 403,
			}
		}
		result.UserID = int(res.UserId)
		result.ErrorCode = int(res.ErrorCode)
		_, err = a.userServiceClientGRPC.GetUserByEmail(ctx, &servicepb.GetUserByEmailRequest{Email: in.Email})
		if err != nil {
			return RegisterOut{
				Status:    http.StatusInternalServerError,
				ErrorCode: 403,
			}
		}
	} else {
		err = a.userJRPC.Call("UserServiceJSONRPC.CreateUser", userCreate, &result)
		if err != nil {
			return RegisterOut{
				Status:    http.StatusInternalServerError,
				ErrorCode: 403,
			}
		}
		err = a.userJRPC.Call("UserServiceJSONRPC.GetUserByEmail", service.GetByEmailIn{Email: in.Email}, &user)
		if err != nil {
			return RegisterOut{
				Status:    http.StatusInternalServerError,
				ErrorCode: 403,
			}
		}
	}

	// _ = a.hash.GenHashString(nil, cryptography.UUID)
	// err = a.verify.Create(ctx, in.Email, hash, user.User.ID)
	// if err != nil {
	// 	return RegisterOut{
	// 		Status:    http.StatusInternalServerError,
	// 		ErrorCode: http.StatusInternalServerError,
	// 	}
	// }
	// go a.sendEmailVerifyLink(in.Email, hash)

	return RegisterOut{
		Status:    http.StatusOK,
		ErrorCode: errors.NoError,
	}
}

func (a *Auth) sendEmailVerifyLink(email, hash string) int {
	userOut := &service.UserOut{}
	var err error
	if a.Ifgrpc {
	} else {
		err := a.userJRPC.Call("UserServiceJSONRPC.GetByEmail", service.GetByEmailIn{Email: email}, &userOut)
		if err != nil {
			return userOut.ErrorCode
		}
	}

	u, err := url.Parse("http://bing.com/verify?email=sample&hash=sample")
	if err != nil {
		a.logger.Fatal("auth: url parse err", zap.Error(err))

		return errors.AuthUrlParseErr
	}
	u.Scheme = "https"
	if a.conf.Environment != "production" {
		u.Scheme = "http"
	}
	u.Host = a.conf.Domain
	q := u.Query()
	q.Set("email", email)
	q.Set("hash", hash)
	u.RawQuery = q.Encode()
	a.notifyEmail(iservice.PushIn{
		Identifier: email,
		Type:       iservice.PushEmail,
		Title:      "Activation Link",
		Data:       []byte(u.String()),
		Options:    nil,
	})

	return errors.NoError
}

// TODO: Refactor
func (a *Auth) notifyEmail(p iservice.PushIn) {
	res := a.notify.Push(p)
	if res.ErrorCode != errors.NoError {
		time.Sleep(1 * time.Minute)
		go a.notifyEmail(p)
	}
}

func (a *Auth) AuthorizeEmail(ctx context.Context, in AuthorizeEmailIn) AuthorizeOut {
	userOut := &service.UserOut{}
	usrOut := &servicepb.UserOut{}
	var err error
	if a.Ifgrpc {
		usrOut, err = a.userServiceClientGRPC.GetUserByEmail(ctx, &servicepb.GetUserByEmailRequest{Email: in.Email})
		if err != nil {
			log.Fatal(err)
		}

		userOut = &service.UserOut{
			User: &m.User{
				ID:       int(usrOut.User.Id),
				Email:    usrOut.User.Email,
				Password: usrOut.User.Password,
			},
			ErrorCode: int(usrOut.ErrorCode),
		}

	} else {
		_ = a.userJRPC.Call("UserServiceJSONRPC.GetUserByEmail", service.GetByEmailIn{Email: in.Email}, &userOut)
		if userOut.ErrorCode > 0 {
			return AuthorizeOut{
				ErrorCode: errors.AuthServiceWrongPasswordErr,
			}
		}
	}
	if err != nil {
		fmt.Println(err)
	}

	user := *userOut.User
	if !cryptography.CheckPassword(user.Password, in.Password) {
		return AuthorizeOut{
			ErrorCode: errors.AuthServiceWrongPasswordErr,
		}
	}
	// if !user.EmailVerified {
	// 	return AuthorizeOut{
	// 		ErrorCode: errors.AuthServiceUserNotVerified,
	// 	}
	// }

	usr := &models.User{
		ID:            user.ID,
		Name:          user.Name,
		Phone:         user.Phone,
		Email:         user.Email,
		Password:      user.Password,
		Role:          user.Role,
		Verified:      user.Verified,
		EmailVerified: user.EmailVerified,
		PhoneVerified: user.PhoneVerified,
	}
	accessToken, refreshToken, errorCode := a.generateTokens(usr)
	if errorCode != errors.NoError {
		return AuthorizeOut{
			ErrorCode: errorCode,
		}
	}
	return AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) AuthorizeRefresh(ctx context.Context, in AuthorizeRefreshIn) AuthorizeOut {
	userOut := &service.UserOut{}
	usrOut := &servicepb.UserOut{}
	var err error
	if a.Ifgrpc {
		usrOut, err = a.userServiceClientGRPC.GetUserByID(ctx, &servicepb.GetByIDRequest{UserId: int32(in.UserID)})
		userOut = &service.UserOut{
			User: &m.User{
				ID:            int(usrOut.User.Id),
				Email:         usrOut.User.Email,
				Name:          usrOut.User.Name,
				Phone:         usrOut.User.Phone,
				Password:      usrOut.User.Password,
				Role:          int(usrOut.User.Role),
				Verified:      usrOut.User.Verified,
				EmailVerified: usrOut.User.EmailVerified,
				PhoneVerified: usrOut.User.PhoneVerified,
			},
			ErrorCode: int(usrOut.ErrorCode),
		}
	} else {
		err := a.userJRPC.Call("UserServiceJSONRPC.GetUserByID", service.GetByIDIn{UserID: in.UserID}, &userOut)
		if err != nil {
			fmt.Println(err)
			return AuthorizeOut{

				ErrorCode: errors.AuthServiceWrongPasswordErr,
			}
		}
	}
	if err != nil {
		fmt.Println(err)
	}
	user := *userOut.User

	usr := &models.User{
		ID:            user.ID,
		Name:          user.Name,
		Phone:         user.Phone,
		Email:         user.Email,
		Password:      user.Password,
		Role:          user.Role,
		Verified:      user.Verified,
		EmailVerified: user.EmailVerified,
		PhoneVerified: user.PhoneVerified,
	}

	accessToken, refreshToken, errorCode := a.generateTokens(usr)
	if errorCode != errors.NoError {
		return AuthorizeOut{
			ErrorCode: errorCode,
		}
	}

	return AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) generateTokens(user *models.User) (string, string, int) {
	accessToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.AccessTTL,
		cryptography.AccessToken,
	)
	if err != nil {
		a.logger.Error("auth: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceAccessTokenGenerationErr
	}
	refreshToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.RefreshTTL,
		cryptography.RefreshToken,
	)
	if err != nil {
		a.logger.Error("auth: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceRefreshTokenGenerationErr
	}

	return accessToken, refreshToken, errors.NoError
}

func (a *Auth) AuthorizePhone(ctx context.Context, in AuthorizePhoneIn) AuthorizeOut {
	return AuthorizeOut{}
}

func (a *Auth) SendPhoneCode(ctx context.Context, in SendPhoneCodeIn) SendPhoneCodeOut {
	panic("asfasf")
}

func (a *Auth) VerifyEmail(ctx context.Context, in VerifyEmailIn) VerifyEmailOut {
	dto, err := a.verify.GetByEmail(ctx, in.Email, in.Hash)
	if err != nil {
		return VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	err = a.verify.VerifyEmail(ctx, dto.GetEmail(), dto.GetHash())
	if err != nil {
		return VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	verifyEmailOut := &VerifyEmailOut{}
	var usrOut *servicepb.UserUpdateResponse
	if a.Ifgrpc {
		usrOut, err = a.userServiceClientGRPC.VerifyEmail(ctx, &servicepb.UserVerifyEmailRequest{UserId: int32(dto.GetUserID())})
		if err != nil {
			return VerifyEmailOut{
				ErrorCode: errors.AuthServiceWrongPasswordErr,
			}
		}
		verifyEmailOut.Success = usrOut.Success
		verifyEmailOut.ErrorCode = int(usrOut.ErrorCode)

	} else {
		err = a.userJRPC.Call("UserServiceJSONRPC.VerifyEmail", service.UserVerifyEmailIn{
			UserID: dto.GetUserID()}, &verifyEmailOut)

		if err != nil {
			return VerifyEmailOut{
				ErrorCode: errors.AuthServiceWrongPasswordErr,
			}
		}
	}

	return VerifyEmailOut{
		Success:   verifyEmailOut.Success,
		ErrorCode: verifyEmailOut.ErrorCode,
	}
}
